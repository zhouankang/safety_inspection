### React使用prop-types验证数据类型

- npm install prop-types安装一个叫prop-types的第三方包
- import PropTypes from 'prop-types'  引入

```javascript
//验证传递过来的值
Todo.propTypes = {
  onClick: PropTypes.func.isRequired,//isRequired表示该值必须要传
  completed: PropTypes.bool.isRequired,//检验布尔值，必传
  text: PropTypes.string，//检验字符串
  list:PropTypes.oneOfType([PropTypes.number,PropTypes.string]),//oneOfType可以验证多个值必须要用[]括起来
  deleItem:PropTypes.func,//检测函数（Function类型）
      
      //func bool string number array object symbol
      
}
```



### connect

- 它不修改传递给它的组件类；而是返回一个新的、连接的组件类供你使用

- 连接React组件与Redux store，连接操作不会改变原来的组件类，反而返回一个新的已与Redux store连接的组件类
- 如果传递的是一个函数，该函数将接收一个dispatch函数，然后由你来决定如何返回一个对象，这个对象通过dispatch函数与action creator以某种方式绑定在一起

```javascript
import React from 'react'
import { connect } from 'react-redux'
import { addTodo } from '../actions/index.js'

const AddTodo = ({ dispatch }) => {
  let input
  return (
    <div>
      <form onSubmit={e => {
        e.preventDefault()
        if (!input.value.trim()) {
          return
        }
        dispatch(addTodo(input.value))
        input.value = ''
      }}>
        <input ref={node => input = node} />
        <button type="submit">
          Add Todo
        </button>
      </form>
    </div>
  )
}

export default connect()(AddTodo)

```



- connect方法接收四个参数，分别是mapStateToProps , mapDispatchToProps , mergeProps , options

```javascript
export default connect(
  mapStateToProps, 
  mapDispatchToProps
)(Link)
```

#### mapStateToProps

- 该方法允许我们将store中的数据作为props绑定到组件中，只要store发生了变化就会调用mapStateToProps方法，mapStateToProps返回的结果必须是一个纯对象，这个对象会与组件的props合并

```javascript
const mapStateToProps = (state, ownProps) => ({
  active: ownProps.filter === state.visibilityFilter
})
```

#### mapDispatchToProps

- 该方法允许我们将action作为props绑定到组件中，如果传递的是一个对象，那么每个定义在该对象的函数都将被当作Redux action creator, 对象所定义的方法名将作为属性名 ， 每个方法将返回一个新的函数 ，函数中的dispatch方法会将action creator的返回值作为参数执行，这些属性会被合并到组件的props中



### combineReducers

- import { combineReducers } from 'redux' 引入

- redux提供了一个combineReducers 方法，用于Reducer的拆分。你只要定义各个子Reducer函数，然后用这个方法，将它们合成一个大的Reducer。

  ```javascript
  import { combineReducers } from 'redux'
  import todos from './todos'
  import visibilityFilter from './visibilityFilter'
  
  export default combineReducers({
    todos,
    visibilityFilter
  })
  //上面的代码通过combineReducers方法将三个子Reducer合并成一个大的函数。
  ```



### reducer (减速器)

- reducer就是一个纯函数，接收旧的state和action,返回新的state.
- 注意：
  - 不要修改state.
  - 在default情况下返回旧的state.遇到未知的action时，一定要返回旧的state
- state={}  或者  state = 默认值

```javascript
const todos = (state = {}, action) => { //state为一个空对象
  switch (action.type) {  //通过监听action返回不同的结果
    case 'ADD_TODO':
      return [
        ...state,
        {
          id: action.id,
          text: action.text,
          completed: false
        }
      ]
    case 'TOGGLE_TODO':
      return state.map(todo =>
        (todo.id === action.id)
          ? {...todo, completed: !todo.completed}
          : todo
      )
    default:
      return state
  }
}

export default todos
```



  ```javascript
import { VisibilityFilters } from '../actions' 

const visibilityFilter = (state = VisibilityFilters.SHOW_ALL, action) => {
    //state接收来自于VisibilityFilters文件的SHOW_ALL默认值，监听action
  switch (action.type) {
    case 'SET_VISIBILITY_FILTER':
      return action.filter
    default:
      return state
  }
}

export default visibilityFilter

  ```

### redux工作流程

 判断store有无，有直接返回，没有去Reducer

- reactComponent(借书者) --> action（图书管理员） --> Store（图书管） --> ReactComponent
- reactComponent(借书者) --> action（图书管理员） --> Store（图书管） --> Reducer（图书管理软件）--> Store -->ReactComponent
- ui组件触发action创建函数 ---> action创建函数返回一个action ------> action被传入redux中间件(被 saga等中间件处理) ，产生新的action，传入reducer-------> reducer把数据传给ui组件显示 -----> mapStateToProps ------> ui组件显示



store.subscribe(函数方法)

函数方法 =>    this.setState(store.getState())



- reducer中的state必须是空对象或者null

- ```
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  在仓库中绑定redux插件redux-devTool
  ```

- 因为不能改变state的值，所以在操作的时候需要JSON.parse(JSON.stringify(state))深拷贝一个值，再将action中传过来的值赋值给新拷贝出来的值





reducer -> 默认值 -> 处理逻辑 -> （state , action ）

strore -> createStore创建仓库 -> 关联reducer

action ->定义type类型 -> 获取参数 -> 传递给store仓库



export 导出的是对象  import { ... } from '.../'  ；  有{}

export defalut 导出的是文件 import ... from '../'  无{}



### saga

安装saga

npm i --save redux-saga

问题：因为saga需要es6或者es7去写，可能会有问题regeneratorRuntime is not defined

```
npm i --save-dev babel-plugin-transform-runtime
在 .babelrc 文件中添加：

   "plugins": [    
       [   
        "transform-runtime",   
        {
             "helpers": false,      
             "polyfill": false,      
             "regenerator": true,      
             "moduleName": "babel-runtime"    
        }  
      ]
    ]
```

```javascript
引入saga
import {createStore ,applyMiddleware , compose } from 'redux'
import reducer from '../reducer/index'
import createSagaMiddleware from 'redux-saga'
import mySaga from './sagas'

const sagaMiddleware = createSagaMiddleware()

const composeEnhandcers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
                          window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose

const enhandcers = composeEnhandcers(applyMiddleware(sagaMiddleware))

  const store = createStore(
    reducer,
    enhandcers
    )

    sagaMiddleware.run(mySaga)
    export default store

//createSagaMiddleware 随便取得常量名
import createMiddleware from 'redux-saga'
const middleware = createMiddleware()
  
```

- yield 等待

- takeEvery 监听 

  - import { takeEvery } from 'redux-saga/effects'  引入

- fork 分叉 ，并发  ，无阻塞执行

- put  发起一个action到store ,是异步的，不会立即触发

  - payload是请求返回的数据

- call  阻塞执行，call()执行完才会往下执行

- select 得到store中的state中的数据

- throttle 节流阀 ，在指定的时间内只执行一次

  - ```jsx
     yield throttle(3000, 'GET_IMAGES', getSagaImage);   // 3s内新的操作将被忽略
     ```

```
阻塞调用 和 非组塞调用
阻塞调用
阻塞调用的意思是： saga 会在 yield 了 effect 后会等待其执行结果返回，结果返回后才恢复执行 generator 中的下一个指令
非阻塞调用
非阻塞调用的意思是： saga 会在 yield effect 之后立即恢复执行

yield put({      // dispatch一个action到reducer， payload是请求返回的数据
        type: actionTypes.GET_AGE_SUCCESS,
        payload: res   
    })

```



### compose增强函数  ————>来自redux

```javascript
const composeEnchancers =  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
       window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose
//1定义一个常量

const enhancer = composeEnchancers(applyMiddleware(saga))
//2同时执行2个函数

const store = createStore(reducer , enhancer)
```

```

import { takeEvery ,put } from 'redux-saga/effects'
import { GET_BY_LIST , GET_BY_LIST_RES } from '../action/indexType'
import axios from 'axios'
function* mySaga() {
    yield takeEvery( GET_BY_LIST , newHandleList )
   
}

function* newHandleList(){
   const res = yield axios.get('https://tearsill.com:8085/Painter/findRecentPainter?pageNum=1&pageSize=11')
   yield put({
     type:GET_BY_LIST_RES,
     data:res.data.data.list
   })
}
  export default mySaga
  
  
  
  https://tearsill.com:8085/Painter/findRecentPainter?pageNum=1&pageSize=11
```



### react-Redux

#### Provider (提供器)

- 被Provider包裹的元素都可以获得store的值
- 来自于react-redux
- <Provide store={store}></Provide>

```javascript
import { Provider } from 'react-redux';

import reducer from './demoRedux/reducer';

import { createStore } from 'redux'

const store = createStore(reducer)
```



#### connect (连接器)

- connect(xxx , null)(index)

  - xxx -->表示映射 ,通常用常量stateToProps

    - 表示将仓库中inputValue的值给props中的inputValue
    - 使用this.props.inputValue === this.state.inputValue

  - null ---> 表示映射，一般是处理dispatch

    ```javascript
    const stateToProps = ( state ) => { return  { defalutValue : state.defalutValue} }
    
    const dispatchToProps = (dispatch) => {
       return {
            inputChange(e){
                const action = {
                    type:"todoList",
                    value:e.target.value
                }
                dispatch(action)
            }
       }
    } 
    
    export default connect(stateToProps,dispatchToProps)(DemoRudex) ;
    ```

    --	-	-	-

- stateToProps,dispatchToProps必须返回纯对象，return {}



connect from react-redux

Provider from react-redux

createsaga from 'redux-saga'

createStore from  redux

applyMiddleware from redux

compose from redux