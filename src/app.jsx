import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import DemoRedux from './demoRedux/index'
import { Provider  } from 'react-redux';
import reducer from './demoRedux/reducer';
import { createStore ,applyMiddleware ,conpose } from 'redux'
import createMiddleSaga from 'redux-saga'
import sagas  from './demoRedux/sagas'

const sagaMiddleware = createMiddleSaga()

const composeEnhandcers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
                          window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : conpose

const echaner = composeEnhandcers(applyMiddleware(sagaMiddleware))

const store = createStore(reducer,echaner)
sagaMiddleware.run(sagas)


class App extends React.Component {
  constructor(props) {
       super(props)
    }

  render() {
    return (
      <div style={{height:'100%'}}>
        <Provider store={store}>
           <DemoRedux />
        </Provider>
           
      </div>
    )
  }
}


export default App

ReactDOM.render(
    <App />
  ,
  document.getElementById('app')
);

/**
 * Route组建里面用render属性替换component来渲染页面，
 * 根据routerMap.js中的每一条路由信息中的auth(自定义)字段来区分是否需要进行登陆拦截，
 * 再根据redux里面的token字段来判断是不是登陆状态，然后进行相关的操作。
 * 如果已经拦截了就把当前的路由通过Redirect的state来传递到登陆页面，
 * 在登陆页面打印this.props来看控制台的输出
*/


