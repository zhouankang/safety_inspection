import {DEFAULT_VALUE , ADD_TODOS ,DELETE_TODOS ,GET_LIST} from './indexType'
export const getInputValue = (value) => ({
    type:DEFAULT_VALUE,
    value
})

export const addTodos = () => ({
    type:ADD_TODOS
})

export const deleteTodo = (index) => ({
    type : DELETE_TODOS,
    index
})

export const getListValue = () => ({
    type:GET_LIST
})