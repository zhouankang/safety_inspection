export const DEFAULT_VALUE = 'inputValue';

export const ADD_TODOS = 'addTodos';

export const DELETE_TODOS = 'deleteTodos';

export const GET_LIST = 'getList';

export const SAGA_TODO_LIST = 'sagaTodoList'