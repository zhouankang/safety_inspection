import { getInputValue , addTodos ,deleteTodo , getListValue} from './action/index'

export  const dispatchToProps = (dispatch) => {
   return {

    handleChange(e){
        const action = getInputValue(e.target.value)
        dispatch(action)
     },

     addTodo(){
         const action = addTodos()
         dispatch(action)
     },
     deleteTodos(index){
        const action =  deleteTodo(index)
        dispatch(action)
     },
     getList(){
         const action = getListValue()
         dispatch(action)
     }
   }
}