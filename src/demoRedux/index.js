import React , {useEffect} from 'react'
import { connect } from 'react-redux'
import { dispatchToProps } from './dispatchToProps'
import { stateToProps } from './stateToProps'

function DemoRudex(props){
    let {handleChange , addTodo ,inputValue ,list ,deleteTodos ,getList } = props
    useEffect(() => {
        getList()
    },[])
        return (
            <div>
              <input 
              onChange={handleChange}
              value={inputValue}
              />
              <button onClick={addTodo}>
                  添加
              </button>
              <ul>
                 {list.map((item , index) => <li key={index} onClick={() => deleteTodos(index)}>{item.uName}</li>)}
              </ul>
            </div>
        )
}

export default connect(stateToProps,dispatchToProps)(DemoRudex)