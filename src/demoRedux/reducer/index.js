
let defaultData = {
    inputValue:'',
    list:[{
        uName:'defalutValue'
    }]
   
}
export default ( state = defaultData, action ) => {
    switch(action.type){
        case 'inputValue' : {
            let newState = JSON.parse(JSON.stringify(state))
            newState.inputValue = action.value
            return newState
        }
        case 'addTodos' : {
            let newState = JSON.parse(JSON.stringify(state))
            newState.list.push({uName:newState.inputValue})
            newState.inputValue = ''
            return newState
        }
        case 'deleteTodos' : {
            let newState = JSON.parse(JSON.stringify(state))
            newState.list.splice(action.index,1)
            return newState
        }
        case 'sagaTodoList' : {
            let newState  = JSON.parse(JSON.stringify(state))
            newState.list = action.data
            return newState
        }
        default : return state
    }
}