import { takeEvery ,put } from 'redux-saga/effects'
import { GET_LIST , SAGA_TODO_LIST } from './action/indexType'
import axios from 'axios'
function* mySaga() {
    yield takeEvery( GET_LIST , newHandleList )
   
}

function* newHandleList(){
   const res = yield axios.get('https://tearsill.com:8085/Painter/findRecentPainter?pageNum=1&pageSize=11')
   yield put({
     type:SAGA_TODO_LIST,
     data:res.data.data.list
   })
}
  export default mySaga