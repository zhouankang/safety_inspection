import {createStore ,applyMiddleware , compose } from 'redux'
import reducer from '../reducer/index'
import createSagaMiddleware from 'redux-saga'
import mySaga from './sagas'

const sagaMiddleware = createSagaMiddleware()

const composeEnhandcers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
                          window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose

const enhandcers = composeEnhandcers(applyMiddleware(sagaMiddleware))

  const store = createStore(
    reducer,
    enhandcers
    )

    sagaMiddleware.run(mySaga)
    export default store
  