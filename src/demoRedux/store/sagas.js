import { takeEvery , put } from 'redux-saga/effects'
import {GET_TODO_LIST , SAGA_TYPE} from '../action/indexType'
import axios from 'axios';
function* sagas(){
   yield takeEvery(GET_TODO_LIST,getTodoHandle)
}

function* getTodoHandle(){
  const res = yield axios.get('https://tearsill.com:8085/Painter/findRecentPainter?pageNum=1&pageSize=11')
  yield put({
    type:SAGA_TYPE,
    data:res.data.data.list
  })
}
export default sagas