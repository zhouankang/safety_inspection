const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack           = require('webpack');

const path = require('path');

module.exports = {
  entry: './src/app.jsx',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/app.js'
  },
 resolve:{
   alias:{
     page: path.resolve(__dirname, 'src/page')
   }
 },
  module: {
    rules: [
      //react文件处理
      {
        test: /\.jsx$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env','react',"stage-0"]
          }
        }
      },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env','react',"stage-0"]
          }
        }
      },
      //css样式处理
      {
        test: /\.css$/,
        use: ['style-loader','css-loader'],
      },
      //less样式处理
      {
        test: /\.less$/,
        use: [
            "style-loader", // creates style nodes from JS strings
            "css-loader", // translates CSS into CommonJS
            "less-loader" // compiles Sass to CSS, using Node Sass by default
        ]
      },
      //图片处理
      {
        test: /\.(png|jpg|gif)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              name:'resource/[name].[ext]'
            }
          }
        ]
      },
      //字体图标
       {
        test: /\.(eot|svg|ttf|woff|woff2)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              name:'resource/[name].[ext]'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    //处理html文件
      new HtmlWebpackPlugin(
          {template:'./src/index.html'}
      ),
     //独立css文件
     new ExtractTextPlugin('css/[name].css'),
    //  //提出公共模块
    //  new webpack.optimize.CommonsChunkPlugin({
    //    name:'common',
    //    filename:'js/base.js'
    //  })
    
    ],
     devServer: {
        contentBase: './dist',
        host: 'localhost', // 服务器的ip地址 希望服务器外可以访问就设置 0.0.0.0
        port: 8082, // 端口
        open: true, // 自动打开页面
        historyApiFallback: true,
        proxy: {
          "/wanhu": {
            "target": "http://10.40.72.108:8082/",
            
            "changeOrigin": true
            }
        }
     },
     optimization: {
      //抽取公共的dm
          splitChunks: {
              cacheGroups: {
                  commons: {
                     name: "commons",
                     chunks: "initial",
                      minChunks: 2
                  }
              }
          }
      },
};